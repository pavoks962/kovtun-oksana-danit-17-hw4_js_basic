"use strict"


let firstNumber;
let secondNumber;
let mathOperation;
do {
    firstNumber = prompt('Enter any first number', firstNumber ? firstNumber : '');
} while (isNaN(firstNumber) || firstNumber == '')
do {
    secondNumber = prompt("Enter any second number", secondNumber ? secondNumber : '');
} while (isNaN(secondNumber) || secondNumber == '')

do {
    mathOperation = prompt("Choose operation: enter + or - or / or *", mathOperation ? mathOperation : '');
} while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '/' && mathOperation !== '*')

/** 
 * @param {number} firstNumber 
 * @param {number} secondNumber 
 * @returns 
 */
const calcMathOperation = (firstNumber, secondNumber) => {

    switch (mathOperation) {
        case '+':
            return +firstNumber + +secondNumber;
            break;
        case '-':
            return +firstNumber - +secondNumber;
        case '/':
            return +firstNumber / +secondNumber;
            break;
        case '*':
            return +firstNumber * +secondNumber;
            break;
    };
}


console.log(calcMathOperation(firstNumber, secondNumber))